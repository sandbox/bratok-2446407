<?php
/**
 * @file 
 *  Библиотека обертка вокруг ctools_wizard_multistep_form для создания 
 *  пошаговых форм используя принцыпы ООП
 * @author 
 *  A. Bratko <bratko_@mail.ru>
 */

/**
 * @defgroup  <test_group> (group title)
 *  Описаниетестовой группы 
 * 
 */

namespace Drupal\rcontext;

class Rcontext{
  static protected $instance = null;
  protected $contextCollection = null; 
    
  static function getInstance(){
    $data = $_SERVER['REQUEST_METHOD'] == 'GET' ? $_GET : $_POST;
    if (!static::$instance && isset($data['rcontext'])){
      static::$instance= new static($data['rcontext']);
    }
    return static::$instance;
  }
  
  protected function __construct($rcontextData) {
    foreach($rcontextData  as $contextName => $contextInfo ){
      $contextValue = $contextInfo['controller']::getInstance($contextInfo['data']);
      $this->set($contextName, $contextValue);
    };
  }
  
  function set($name, $value){
    $this->contextCollection[$name] = $value;
    return $this;
  }
    
  function get($name, $default = null){
    return (isset($this->contextCollection[$name])) 
      ? $this->contextCollection[$name]
      : $default;
  }
}




 