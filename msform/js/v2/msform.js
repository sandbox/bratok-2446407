/**
 * @file
 * @author A.Bratko <bratko_@mail.ru>
 */


(function($){
  var Controller = function(){
    
  }
  
  var MsForm = {};
  MsForm.options = {} 
  MsForm._create =  function() {
    this._onEventHandler();
    this._super();
  }
  
  MsForm._onEventHandler = function(){
    var options = this.options;
    if (Boolean(options.eventHandlers)){
      Drupal.UiEventController.onEventHandlers(this, options.eventHandlers) 
    }
  }  
  
  MsForm._offEventHandler = function(){
    var options = this.options;
    if (Boolean(options.eventHandlers)){
      Drupal.UiEventController.offEventHandlers(this, options.eventHandlers) 
    }
  }  
  
  MsForm._destroy = function() {
    this._offEventHandler();
  }
  
  MsForm.updateForm = function(event, data){
    $(this.element).html(data);
    var settings = Drupal.settings;
    Drupal.attachBehaviors(this.element, settings);
  }
  
  MsForm.showMessage = function(event, data){
    if (event.data.eventGenerator != $(event.target).attr('id')){
      return true;
    }
    
  }
  
  MsForm.redirect = function(event, data){
    if (event.data.eventGenerator == $(event.target).attr('id')){
      if (Boolean(event.data.url)){
        window.location = event.data.url;
      }
    }
  }
  
  MsForm.getId = function(){
    return $(this.element).attr('id');
  }
  
  $.widget('custom.msForm', MsForm);
  
  Drupal.behaviors.msform = {
    attach: function (context, settings) {
      $('div.msform', context).once('msform' , function(index, element){
        var elementSettings = null;
        if (Boolean(settings.msform)){
          elementSettings = settings.msform[$(element).attr('id')];
        }
        $(element).msForm(elementSettings);
        
      });
              
    }
  }
  
  
})(jQuery)

//# sourceURL=/msfrom/v2/msfrom.js