<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Drupal\msform\v2;

class MsFormHandler{
  protected $params = null;
  protected $callback  = null;
  protected $result = null;
  
  public function __construct($callback, $paramArr = null) {
    ;
  }
  
  function getResult(){
    $this->execute();
    return $this->result;
  }
  
  function execute(){
    $this->result = call_user_func_array($this->callback, $this->params);
    return $this;
  }
}

