<?php
/**
 * @file 
 *  Библиотека обертка вокруг ctools_wizard_multistep_form для создания 
 *  пошаговых форм используя принцыпы ООП
 * @author 
 *  A. Bratko <bratko_@mail.ru>
 */

/**
 * @defgroup  <test_group> (group title)
 *  Описаниетестовой группы 
 * 
 */

namespace Drupal\msform\v2;

use Drupal\jqdialog\v1\UiEvent as UiEvent;  
use Drupal\jqdialog\v1\ui\Button as Button; 

class MsForm{
  
  static $basePath  = 'msform/v2';
  static $vertion   = 'v2';
  static $defaultSettings   = array(
    'display_form_title' => '',
  );
  
  protected $id = null;
  protected $settings = null;
  protected $buildId  = null;
  protected $stepId = null;
  protected $forms  = null;
  protected $isNew  = null;
  protected $isChanged  = null;
  protected $options = array();
  protected $destroyHimself = false;
  protected $formValues = array();
  protected $contextOfBuild = array();
  protected $actionHandlers = array();
  
  /**
   * Режим одной формы.
   * В режиме одной формы каждая форма из многошаговой формы может
   * быть получена и обаботана как единственная и независимая форма. 
   * Отсутвствуют кнопки перехода между шагами. 
   * При этом после обработки формы будут выполнены 
   * общие обработчики все  пошаговой формы 
   * @var type 
   */
  protected $oneFormMode = null;
  
  
  /**
   * Признак видимости кнопки "Сохранить"
   *  - true - видна на любом шаге. Клик приводит к сохранению изменения и закрытию диалога.
   *  - false - видна только на последнем шаге 
   * @var type 
   */
  protected $finishBtnIsVisible = false;
  
  /**
   * Признак существования следующего шага 
   * @var type 
   */
  protected   $hasNextForm = false;
  /**
   * Имя действие по умолчанию
   * @var type 
   */
  protected $startActionName = 'start';
  
  /**
   * Имя действие завершающего диалог с выполнением обработчиков и ео закрытием 
   * @var type 
   */
  protected $finishActionName = 'finish';
  
  /**
   * Префикс используемый для формирования имени метода 
   * для обработки запрошенного action 
   * @var type 
   * @see MsForm::setAction()
   */
  protected $actionHandlerPrefix = 'goTo';
  
  /**
   * Имя метода обработчика текущего запроса 
   * @var type 
   */
  protected $actionHandler = null;
  /**
   * Редактируемые объекты, которые являются постоянными.
   * Сохранются внутри объекта пошаговой формы 
   * и не могут быть изменены со стороны клиента . 
   * Т.е. форма всегда  открывается в режиме редактирования 
   * объектов указанных в этом массиве 
   * @var type 
   */
  protected $permanentEditableObject  = array();
  
  /**
   * Динамически редактируемые объекты. 
   * Объекты указанные в массите, формируют доплнительные параментры URL.
   * Форма открывается в режме редактирвоания объетов 
   * указанных в параметрах URL, если они доступны для редактирвоания 
   * Т.О. одна форма может быть интерфейсом для редактирования  
   * нескольких объектов, каких именно неизветсно за ранее 
   * @var type 
   */
  protected $varialbleEditableObject  = array();
  
  /**
   * Хранит текущие динамически редактируемые объекты 
   * формируется из параметров URL при старте диалога и до его окончания
   * @var type 
   */
  protected $editableObject  = array();
  
  static  protected $msfromcCollection  = array();
  /**
   * Команды для UI 
   * @var type 
   */
  protected $uiCommands  = array();
  
  /**
   * Признак того что диалог спользователем уже начат.
   * Т.е. мы уже отобразили первую форму 
   * 
   * Пошагаовая форма может быть создана, 
   * но диалог с пользователем может быть не начат, 
   * например в случае формирования формы внутридиалогового окна
   * открываемого по кнопке.
   * 
   * В этом случае объект пошаговой формы создается на момент
   * формирования страницы, но начинается после отображения 
   * первого шага. 
   * 
   * @var type 
   */
  protected  $isAlreadyRunning = false;
  
  static function newInstance($id, $buildId = null){
    $instance = new static($id, $buildId);
    static::$msfromcCollection[$instance->getId()] = $instance;
    return  $instance;
  }
  
  static function getInstance($id, $create = true){
    if (!isset(static::$msfromcCollection[$id]) 
        && $instance = static::getFromStorage($id)){
      static::$msfromcCollection[$id] = $instance;
    }   
    else if (!isset(static::$msfromcCollection[$id]) && $create){
      static::newInstance($id);
    }
    return static::$msfromcCollection[$id];
  }
  
  
  
  static protected function getFromStorage($id) {
    global $user;
    $instance = null;
    $sql = 'select data from msform_build'
      . ' where'
      . '   msform_id = :msform_id'
      . '   and sid = :sid';
    $param = array(
      ':msform_id' => $id,
      ':sid' => isset($user->sid) ? $user->sid:  session_id(),
    );
    $res = db_query($sql, $param)->fetchAssoc();
    if ($res){
      $instance = unserialize($res['data']);
    }
    return $instance;
  }
  
  function getBuildId(){
    return $this->buildId;
  }
  
  function isAccessible(){
    try{
      $this->checkAccessToEditableObject();   
    }
    catch(exc\EditableObjectAccessDenied $e){
      return false;
    }
    return true;
  }
  
  
  function checkAccessToEditableObject(){
  }
  
  
  static protected function saveToStorage($instance) {
    global $user;
    $sql = 'INSERT INTO {msform_build} '
      . ' (id, msform_id, data, created, sid )'
      . 'VALUES '
      . ' (:id, :msform_id, :data, UNIX_TIMESTAMP(), :sid)'
      . 'ON DUPLICATE KEY UPDATE '
      . ' data       = VALUES(data)'
      . ' , created  = VALUES(created)'
      . ' , msform_id  = VALUES(msform_id)';
    $args = array(
      ':id'         => hash('md5', $instance->getId().$instance->sid), 
      ':msform_id'  => $instance->getId(), 
      ':data'       => serialize($instance), 
      ':sid'        => $instance->sid, 
    );
    $res = db_query($sql, $args);
  }
  
  
  static protected function delFromStorage($instance) {
    global $user;
    $sql = 'DELETE FROM {msform_build} '
      . ' WHERE '
      . '   sid = :sid'
      . '   and msform_id = :msform_id';
    $args = array(
      ':msform_id'  => $instance->getId(), 
      ':sid'        => $instance->sid, 
    );
    $res = db_query($sql, $args);
  }
  
  function __construct($msformId) {
    global $user;
    $this->id   = $msformId;
    $this->sid  = isset($user->sid)?$user->sid: session_id();
    $this->buildId = uniqid();
    $this->isNew = true;
//    $this->contructButtons();
  }
  
  function __destruct() {
    if (!$this->destroyHimself){
      $this->uiCommands = array();
      static::saveToStorage($this);
    }
    else {
      static::delFromStorage($this);
    }
  }
  
  protected function contructButtons(){
     $this->buttons = array(); 
     $this->buttons['cancel'] = Button::newInstance(t('Cancel'));
  }
  
  function getButton($btnId){
    return $this->buttons[$btnId];
  }
  
  
  function getId(){
    return $this->id; 
  }

  function getMsFromId() {
    return $this->msformId;
  }
  
  function getOptions(){
    return $this->options;
  }
  
  public function hasNext(){
    $next = next($this->forms);
    if ($next!== false){
      prev($this->forms);
    }
    return $next;
  }
  
  public function hasPrev(){
    $prev = prev($this->forms);
    if ($prev!== false){
      next($this->forms);
    }
    return $prev;
  }
  
  public function getNext(){
    return key($this->forms);
  }
  
  public function getCurrentStep(){
    return key($this->forms);
  }
  
  public function getFirstStep(){
    reset($this->forms);
    return key($this->forms);
  }
  
  public function getEndStep(){
    end($this->forms);
    return key($this->forms);
  }
  
  function setFirstStep(){
    $firstStep = $this->getFirstStep();
    $this->setCurrentStep($firstStep);
    reset($this->forms);
    return $this;
  }
  
  public function setPrevStep() {
    prev($this->forms);
    return $this;
  }
  
  public function setNextStep() {
    
    next($this->forms);
    return $this;
  }
  
  public function setCurrentStep($stepId){
    $this->isAlreadyRunning = (bool)$stepId;
    if ($stepId && !isset($this->forms[$stepId])){
      throw new exc\InnerFormNotFound();
    }
    while(key($this->forms) !== $stepId){
      next($this->forms);
    }
    $this->stepId = $stepId;
    return $this;
  }
  
  /**
   * Устанавливает текущий обработчик запроса, 
   * делая предварительную проверку доструности вызова и обработки 
   * @param type $action
   * @return \Drupal\msform\v2\MsForm
   * @throws exc\MsFormActionNotFound
   */
  public function setActionHandler($action = null){
    if ($action == $this->finishActionName // прилетел запрос на выполенение обработчиков и закрытие формы
        && $this->hasNextForm  // и текущая форма не последняя 
        && !$this->finishBtnIsVisible // но мы ожидаем этоот запрос только когда текущая форма последняя 
        && !$this->oneFormMode	
        ){
      throw new exc\MsFormActionNotFound();
    }
    $action = $action ?: $this->startActionName;
    
    $methodName = $this->actionHandlerPrefix . ucfirst($action);
    if (!method_exists($this, $methodName)){
      throw new exc\MsFormActionNotFound();
    }
    $this->actionHandler  = $methodName;
    return $this;
  }
  
  public function setFinishBtnIsVisible($visible) {
    $this->finishBtnIsVisible = $visible;
    return $this;
  }
  
  public function getFinishBtnIsVisible() {
    return $this->finishBtnIsVisible;
  }
  
  public function getActionHandler(){
    return $this->actionHandler;
  }
  
  public function processRequest(){
    $action = $this->getActionHandler();
    return  $this->$action();
  }
  
  function processCurrentForm(){
    $form = $this->getForm();
    if (!$form->build()->isExecuted()){
      throw new exc\InnerFormNotExecuted();
    }
    return $this;
  }
  
  function saveFormValues(){
  }
  
  function setFormValues($formValues = null, $stepId = null){
    $formValues = ($formValues)?:$this->getForm()->getValues();
    $stepId = ($stepId)?:$this->getForm()->getId();
    $this->formValues = $formValues + $this->formValues;
    return $this;
  }
  
  function getFormValues(){
    return $this->formValues;
  }
  
  function getFormValue($key, $default = null){
    return isset($this->formValues[$key])
      ? $this->formValues[$key] 
      : $default;
  }
  
  function setFormValue($key, $value){
    $this->formValues[$key] = $value;
    return $this;
  }
  
  
  function delStepValues( $stepId = null){
    $stepId = ($stepId)?:$this->getCurrentStep();
    unset($this->stepValues[$stepId]);
  }
  
  function getFieldValues(){
    
  }
  
  function goToNext(){
    $test = '';
    try{
      $form = $this
        ->processCurrentForm()
        ->setFormValues() //< @todo это должно быть в обработчике результатат формы 
        ->setNextStep()
        ->getForm()
        ->build();
      
      $nextCommand = $this
        ->getEvent('update', $form->render())
        ->toCommand('trigger');
      
      return $this
        ->setUiCommand('next', $nextCommand)
        ->getUiCommand();
      
    }
    
    catch(exc\InnerFormHasErrors $e){
      $form = $this->getForm();
      $command = $this
        ->getEvent('update', $form->render())
        ->toCommand('trigger');
      
      return $this
        ->setUiCommand('update', $command)
        ->getUiCommand();
    }
    catch(InnerFormNotExecuted $e){
      return $form;
    }
  }

  

  static function initJsLibrary(){
    static $isCalled  = false;
    if (!$isCalled){
      drupal_add_library('system', 'ui');
      $path = drupal_get_path('module', 'msform');
      drupal_add_js( $path.'/js/'.static::$vertion.'/msform.js', 'file');
      $isCalled  = true;
    }  
  }
  
  function initJs(){
    static $isCalled  = false;
    if ($this->isChanged || !$isCalled){
      static::initJsLibrary();
      $settings['msform'][$this->getId()] = $this->getOptions();
      drupal_add_js($settings, 'setting');
    }
    return $this;  
  }
  
  /**
   * Генерирует HTML представление формы 
   * @return string 
   * @todo 
   * Переделать на использование системы темизации Drupal 
   */
  function render(){
    $test = '';
    $formRender = $this
      ->on($this->getEvent('update'), 'updateForm')
      ->on($this->getEvent('finish'), 'finishForm')
      ->initJs()
      ->getForm()->build()->render();
    
    return '<div id="'.$this->getId().'" class="msform"> '
      . '<div class="msfrom-form">'
      .   $formRender  
      . '</div>'
      .'</div>';
  }
  
  function getCommonTitle(){
    return token_replace($this->commonTitle, array('msform' => $this));
  }
  
  function setCommonTitle($title){
    $this->commonTitle = $title;
    return $this;
  }
  
  function goToStart(){
    return $this
      ->resetHimself()
      ->loadEditableObject()
      ->setFirstStep();
  }
  
  function goToForm(){
    $test ='';
    try{
      $finishCommand = $this
        ->getEvent('finished')->toCommand('trigger');
      
      $this
        ->processCurrentForm()
        ->setFormValues()
        ->applyHandlers('finish')
        ->setUiCommand('finished', $finishCommand)
        ->getUiCommand();
      
      $form = $this->getForm();
      $command = $this
        ->getEvent('update', $form->render())
        ->toCommand('trigger');
      
      $this->resetHimself();
      
      return $this
        ->setUiCommand('update', $command)
        ->getUiCommand();
      
    }
    catch(exc\InnerFormHasErrors $e){
      $form = $this->getForm();
      $command = $this
        ->getEvent('update', $form->render())
        ->toCommand('trigger');
      
      return $this
        ->setUiCommand('update', $command)
        ->getUiCommand();
    }
    catch(exc\InnerFormNotExecuted $e){
      return $this;
    }
  }
  
  function loadEditableObject(){
    if (isset($_GET['edit'])){
      $this->editableObject = $_GET['edit'];
    }
    return $this;
  }
  
  function goToBack(){
    $form = $this
      ->setPrevStep()
      ->getForm()
      ->build();
    
    $backCommand = $this
      ->getEvent('update', $form->render())
      ->toCommand('trigger');
      
    return $this
      ->setUiCommand('back', $backCommand)
      ->getUiCommand();
  }
  
  function getActionHandlers($action){
    return $this->actionHandlers[$action];
  }
  
  function goToCancel(){
    $command = $this
      ->getEvent('cancel')->toCommand('trigger');
    $commands = $this
      ->applyHandlers('cancel')
      ->setUiCommand('cancel', $command)
      ->getUiCommand();

    $this->resetHimself();
    return $commands;
  }
  
  function afterFinsh(){
    return $this
        ->getEvent('update', $this->getFinishMessage())
        ->toCommand('trigger');
  }
  
  function setFinishMessage($message){
    $this->finishMessage = $message;
    return $this;
  }
  
  function getFinishMessage(){
    return $this->finishMessage;
  }
  
  function addHandler($operationType, $className, $methodName){
//    $this->handler[$operationType][] = 
    
    
  }
  
  function applyHandlers($actionId){
    $handlers = $this->getHandlers($actionId);
    foreach($handlers as $name => $handler){
      $callback = $handler->callback;
      $params = isset($handler->params) 
        ? $handler->params
        : array();
      array_unshift($params, $this);
      $result = call_user_func_array($callback, $params);
    }
    return $this;
  }
  
  function getHandlers($actionId){
    return isset($this->actionHandlers[$actionId]) 
      ? $this->actionHandlers[$actionId]
      : null;
  }
  
  function goToFinish(){
    $test ='';
    try{
      $finishCommand = $this
        ->getEvent('finished')->toCommand('trigger');
      $commands = $this
        ->processCurrentForm()
        ->setFormValues()
        ->applyHandlers('finish')
        ->setUiCommand('finished', $finishCommand)
        ->getUiCommand();
      
      $this->resetHimself();
      return $commands;
    }
    catch(exc\InnerFormHasErrors $e){
      $form = $this->getForm();
      $command = $this
        ->getEvent('update', $form->render())
        ->toCommand('trigger');
      
      return $this
        ->setUiCommand('update', $command)
        ->getUiCommand();
    }
    catch(exc\InnerFormNotExecuted $e){
      return $this;
    }
  }
  
  public function pushForm($form, $stepId = null){
    $stepId = $stepId ?: uniqid();
    $this->forms[$stepId] = $form;
    $this->isChanged = true;
    return $this;
  }
  
  protected function validateSave(){
    
  }
  
  public function save(){
    $this->validateSave();
    self::saveToStorage($this);
//    if ($this->isNew || $this->isChanged){
//      $this->isNew     =
//      $this->isChanged = false;
//      self::saveToStorage($this);
//    }
    return $this;
  }
  
  function isNew(){
    return $this->isNew;
  }
  
  function getUrl($partName = null ){
    $url = self::$basePath . '/' . $this->getId();
    if ($stepId = $this->getCurrentStep()){
      $url .= '/' . $stepId;
    }
    $data = array('edit' => $this->varialbleEditableObject);
    $url = array(
      'path'   => $url, 
      'query'  => $data, 
      'prefix' => '',
    );
    return ($partName) ? $url[$partName] : $url;
  }
  
  function getFormClass(){
    return 'DrupalFormWrapper' ;
  }
  
  function getForm(){
    if (!($stepId = $this->getCurrentStep())){
      $stepId = $this->getFirstStep();
    };    
    return $this->forms[$stepId]->setMsForm($this);
  }
  
  
  protected function getEventPrefix(){
    static $prefix = null;
    if (!$prefix){
      $prefix = str_replace('\\', '.' , __CLASS__);
    }
    return $prefix; 
  }
  
  function getEvent($eventName, $data = null) {
    $eventName = $this->getEventPrefix() . '.' . $this->getId() . '.' .$eventName;
    return UiEvent::getInstance($eventName, $this->getId(), $data);
  }
  
//  function on($event, $eventHandler, $data = array()){
//    $eventName = (is_object($event))
//      ? $event->getName()
//      : $event;
//    $eventTarget = $event->getTarget(); 
//    $data = $data + array('target' => $eventTarget);
//    $this->options['eventHandlers'][$eventName][$eventHandler] = $data;
//    return $this;
//  }
  
  function on(UiEvent $event, $eventHandler, $data = array(), $attachAfterHdName = null, $attachAfterHdOwner = null){
    $eventName = (is_object($event))
      ? $event->getName()
      : $event;
    $eventGenerator = $event->getTarget(); 
    
    $data = $data + array(
      'eventGenerator' => $eventGenerator, 
    );
    if ($attachAfterHdName){
      $data['attachAfter'] = (object)array(
        'handlerName' => $attachAfterHdName, 
        'handlerOwner' => is_object($attachAfterHdOwner) ? $attachAfterHdOwner->getId() : $attachAfterHdOwner, 
      );
    }
    $this->options['eventHandlers'][$eventName][$eventHandler] = $data;
    return $this;
  }
  
  function off(string $event, string $eventHandler = null){
    return $this;
  }
  
      
  function loadContextOfBuild(){
    $data = $_SERVER['REQUEST_METHOD'] == 'GET' ? $_GET : $_POST;
    if (isset($data['contextOfBuild'])){
      $this->contextOfBuild = $data['contextOfBuild'];
    }
    return $this;
  }    
  
  function setEditableObject($name, $value, $variable  = false){
    if ($variable){
      $this->varialbleEditableObject[$name] = $value;
    }
    else {
      $this->permanentEditableObject[$name] = $value;
    }
    return $this;
  }
  
  function getEditableObject($name, $default = null){
    $res = $default;
    if (isset($this->editableObject[$name])){
      $res = $this->editableObject[$name];
    }
    else if (isset($this->permanentEditableObject[$name])){
      $res = $this->permanentEditableObject[$name];
    }
    return $res;
  }
  
  function setOneFormMode($state = true){
    $this->oneFormMode = (bool)$state;
    return $this;
  }
  
  function isOneFormMode(){
    return (bool)$this->oneFormMode;
  }
  function setDescription(){
    
  }
  
  function getDescription(){
    
  }
  
  function addActionHandler($actionId, $callback, $paramsArr = null){
    $callbackKye = hash('md5', $actionId . $callback);
    if (!isset($this->actionHandlers[$actionId][$callbackKye])){
      $this->actionHandlers[$actionId][$callbackKye] = (object)array(
        'callback' => $callback,
        'params' => $paramsArr,
      );
    }
    return $this;
  }
  
  
  function  getUiCommand($name = null){
    if ($name){
      return isset($this->uiCommands[$name]) ? $this->uiCommands[$name] : null;
    }
    else {
      return $this->uiCommands;
    }
    
  }
  
  function  setUiCommand($name, $command){
    $this->uiCommands[$name] = $command;
    return $this;
  }
  
  function resetHimself(){
    $this->formValues = array();
    $this->uiCommands = array();
    $this->editableObject = array();
    return $this;
  }
  
  function setEditMode($val){
    $this->editMode = (bool)$val;
    return $this;
  }
  
}







 