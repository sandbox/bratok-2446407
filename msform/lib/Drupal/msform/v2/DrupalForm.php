<?php
/**
 * @file 
 *  Библиотека обертка вокруг ctools_wizard_multistep_form для создания 
 *  пошаговых форм используя принцыпы ООП
 * @author 
 *  A. Bratko <bratko_@mail.ru>
 */

/**
 * @defgroup  <test_group> (group title)
 *  Описаниетестовой группы 
 * 
 */

namespace Drupal\msform\v2;


class DrupalForm{
  static protected  $forms = array();
  protected $formId = null;
  protected $formState = null;
  protected $formConstructor = null;
  protected $msfrom = null;
  protected $values = null;
  public $state = null;


  static function getInstance($constructorName, $title = null){
    if (!isset(static::$forms[$constructorName])){
      static::$forms[$constructorName] = new static($constructorName, $title);
    }
    return static::$forms[$constructorName];
  }
  
  public function __construct($formId, $title) {
    $this->id = $formId;
    $this->formConstructor = $formId;
    $this->title  = $title;
    $this->state  = array();
  }
  
  public function __sleep() {
    return array('formConstructor', 'title', 'formId');
  }
  
  function getModelConstructor(){
    return $this->formConstructor;
  }

  function getId(){
    return $this->id;
  }
  
  public function setMsForm($msform) {
    $this->msfrom = $msform;
    $this->state['msform'] = array(
      'id' => $msform->getId(), 
      'class' => get_class($msform),
    );
    return $this;
  }
  
  public function getMsForm() {
    if (isset($this->msfrom)){
      return $this->msfrom;
    }
    else {
      $msformInfo  = $this->formState['msform'];
      $className = $msformInfo->class;
      return  $className::getInstance($msformInfo->id);
    }
  }
  
  public function getBuildId() {
    return $this;
  }
  
  function getTitle(){
    return $this->title;
  }
  
  function setTitle($title){
    $this->title = $title;
    return $this;
  }
  
  public function &getState() {
    return $this->state;
  }
  
  function getWrapper(){
    return get_class($this).'::formWrapper';
  }
  
  function setState(&$formState){
    $this->state = $formState;
    return $this;
  }
  function getStatePreparedForBuild(){
    $formState = $this->getState();
    $formState['wrapper_callback'] = $this->getWrapper();
    $formState['no_redirect'] = true; 
    $formState['cache'] = true; 
    unset($formState['input']); 
    return $formState;
  }
  
  public function build(){
    $formState = $this->getStatePreparedForBuild();
    $modelConstructor = $this->getModelConstructor();
    $this->buildForm = drupal_build_form($this->getModelConstructor(), $formState);
    $this->setState($formState);
    if ($this->hasErrors()){
      throw new exc\InnerFormHasErrors();
    }
    return $this;
  }
  
  function hasErrors(){
    return (bool)form_get_errors();
  }
  
  function getBuild(){
    return $this->buildForm;
  }
  /**
   * @todo разобраться что возвращать $form_state['value'] или $this->value
   * @return type
   */
  function getValues(){
    
//    $state = $this->getState();
//    return $state['values'];
//    $state = $this->getState();
    if (!$this->values) $this->values = array();
    return $this->values;
  }
  
  function setValue($key, $value){
    $this->values[$key] = $value;
    return $this;
  }
  
  function getValue($key){
    return $this->values[$key];
  }
  
  function isExecuted(){
    $state = $this->getState();
    return isset($state['executed']) ? $state['executed'] : false;
  }
  
  function isSubmited(){
    $state = $this->getState();
    return isset($state['process_input']) ? $state['process_input'] : false;
  }
  
  function getClickedButton(){
    $state = $this->getState();
    return $state['clicked_button'];
  }
  
  function render(){
    
    $title = ' <div class="msform-form-title">'
      . $this->getTitle()
      . '</div>';
    return $title . theme('status_messages') . drupal_render($this->buildForm);
  }
  
  static function formWrapper($form, &$form_state){
    if ($form_state['msform']){
      $msform = $form_state['msform']['class']::getInstance($form_state['msform']['id']);
      $url = $msform->getUrl('path');  
    }

    $form['#action'] = url($url);
    $form['msform_buttons']  = array('#weight' => 1000000);
    $msformButtons = &$form['msform_buttons'];
    $oneFormMode = $msform->isOneFormMode();
    if ($msform->hasNext() && !$oneFormMode){
      $msformButtons['next'] = array(
        '#type'   => 'submit' ,
        '#value'  => t('Next'),
        '#weight' => 1000, 
        '#ajax' => array(
          'path' => $url .'/next',
        ),
      );
    }
    else {
      $msformButtons['save'] = array(
        '#type'   => 'submit' ,
        '#value'  => t('Save'),
        '#weight' => 1010, 
        '#ajax' => array(
          'path' => $url .'/' .(!$oneFormMode ? 'finish' : 'form'),
        ),
      );
    }
    if ($msform->getFinishBtnIsVisible() && !isset($msformButtons['save'])){
      $msformButtons['save'] = array(
        '#type'   => 'submit' ,
        '#value'  => t('Save and exit'),
        '#weight' => 1010, 
        '#ajax' => array(
          'path' => $url .'/finish',
        ),
      );
    }
    
    if ($msform->hasPrev() && !$oneFormMode){
      $msformButtons['back'] = array(
        '#type'   => 'submit' ,
        '#weight' => 900, 
        '#value'  => t('Back'),
        '#ajax' => array(
          'path' => $url .'/back',
        ),
      );
    }
    
    /**
     * кнопка "Отмена" - показываем всегда 
     */
    $msformButtons['cancel'] = array(
        '#type'   => 'submit' ,
        '#weight' => 1020, 
        '#value'  => t('Cancel'),
        '#ajax' => array(
          'path' => $url .'/cancel',
        ),
      );
    
    return $form;
  }
} 



