<?php
/**
 * @file 
 *  Библиотека обертка вокруг ctools_wizard_multistep_form для создания 
 *  пошаговых форм используя принцыпы ООП
 * @author 
 *  A. Bratko <bratko_@mail.ru>
 */

/**
 * @defgroup  <test_group> (group title)
 *  Описаниетестовой группы 
 * 
 */

namespace Drupal\msform\v2;

class MsFormController {
  static protected $storage = null;
  static $bin = 'cache_form';
  
  protected $dialogCollection = array();
  static protected $selfInstance = null;
  
  static function getInstance(){
    if (!static::$selfInstance){
      static::$selfInstance = new self();
    }
    return static::$selfInstance;
  }
  protected function __construct() {
  }
    
  
  function newMsFrom($msfromId, $className = 'Drupal\jqdialog\v1\JqDialog'){
    $msfrom = new $className($msfromId);
    $msfrom->setController($this);
    $this->msformCollection[$msfromId] = $msfrom ;
    return $this->msformCollection[$msfromId];
  }
  
  function getBasePath(){
    return 'msform/v2';
  }
  
  function getUrl($dialog){
    $base = $this->getBasePath();
    return $base . '/' . $dialog->getId();
  }
  
    
  
}
 

