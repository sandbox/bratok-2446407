<?php
/**
 * @file 
 *  Библиотека обертка вокруг ctools_wizard_multistep_form для создания 
 *  пошаговых форм используя принцыпы ООП
 * @author 
 *  A. Bratko <bratko_@mail.ru>
 */

/**
 * @defgroup  <test_group> (group title)
 *  Описаниетестовой группы 
 * 
 */

namespace Drupal\msform\v1;
//use Drupal\msform\MsForm as MsForm; 

class MsFormRegister{
  static $bin = 'cache_form';
  static function add(MsForm $msform){
    $cid = $msform->getId();
    self::clear($cid);
    if (!self::get($cid)){
      cache_set($cid, $msform, self::$bin, 20*60);
    }
  }
  
  static function get($cid){
    $cache = cache_get($cid, self::$bin);
//    if (!$cache){
////      $class_name = MsForm::getClassName($cid);
//      try{
//        $result = new $class_name();
//      }
//      catch (Exception $e){
//        throw new Exception(403);
//      }
//    }
//    else {
    $result = null;
    if ($cache){
      $result = $cache->data; 
    }
    return $result; 
  }
  
  static function clear($cid){
    cache_clear_all($cid, self::$bin);
  }
}
 