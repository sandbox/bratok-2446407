<?php
/**
 * @file 
 *  Библиотека обертка вокруг ctools_wizard_multistep_form для создания 
 *  пошаговых форм используя принцыпы ООП
 * @author 
 *  A. Bratko <bratko_@mail.ru>
 */

/**
 * @defgroup  <test_group> (group title)
 *  Описаниетестовой группы 
 * 
 */

namespace Drupal\msform\v1;

class MsFormCache{
  protected $_type = null;
  protected $_data = null;
  
  function __construct($name, $data = null) {
    $this->_type = __CLASS__;
    $this->_name = $name;
    
    if ($data){
      $this->_data = $data;
    }
    else {
      ctools_include('object-cache');
      $this->_data = ctools_object_cache_get($this->_type, $this->_name);
    }
  }
  
  function setType($type){
    $this->_type = $type;
    return $this;
  }
  
  
  function  set($name, $value){
    $this->_data[$name] = $value;
    return $this;
  }
  
  function getAll(){
    return ($this->_data)?$this->_data:array(); 
  }
  
  function get($name){
    return isset($this->_data[$name])?$this->_data[$name]:null;
  }
//  function __set($name, $value) {
//    if ($name == '_data'){
//      $this->_data = $value;
//    }
//    else {
//      $this->_data[$name] = $value;
//    }
//  }
  
//  function __get($name) {
//    return isset($this->_data[$name])?$this->_data[$name]:null;
//  }
  
  function save(){
    ctools_include('object-cache');
    ctools_object_cache_set($this->_type, $this->_name , $this->_data);
  }
  
  function clear(){
    ctools_include('object-cache');
    ctools_object_cache_clear($this->_type, $this->_name);
  }
}

abstract class MsForm{
  
  public $step = null;
  public $state = null;
  public $options = null;
  protected $pressedButton =null;
  static protected $regiser = null;
  
  protected $_id = null;
  static protected $default_info = array(
    'class' => '', 
    'cache' => '', 
  );
  /**
   * Получает фолрму по id 
   * 
   * @param string $msform_id 
   *  формируется автоматически на основании имени класса
   *  У никально в рамках всей сисетемы
   *  По этому значению система находит ранее созданную  с помощью класса форму 
   * @param type $settings
   */
  static function get($msform_id, $settings = null){
  } 
  
  function getPressedButton(){
    return $this->pressedButton;
  }
  
  protected function setPressedButton($btn){
    $this->test2 = '222222';
    $this->pressedButton = $btn;
  }
    
  function getCache(){
    if (!isset($this->_cache)) {
      $this->_cache = new MsFormCache($this->id);
    }
    return $this->_cache;
  }
  
  /**
   * Создает и присывивает id форма на основе имени класса 
   * @return type string
   */
  function getId(){
    if (!isset($this->id) || !$this->id){
      $class_name = get_class($this);
      $explode_class = explode("\\", $class_name);
      array_shift($explode_class);
      $this->id = implode('-', $explode_class);
    }
    return $this->id; 
  }
  
  static function getClassName(){
    
  }
  
  function __construct($options = null) {
    $id = $this->getId();
    $this->cache = (isset($options['cache controller']))
     ? new $options['cache controller']($id)
     : new MsFormCache($id);
    
    if (isset($options['ajax'])){
      $options['ajax'] = true;
    }
      
    $options['id'] = $id;
    $options['ajax'] = true;
    $this->options = $options;
    if (isset($_GET['modal'])){
      $this->modal = $_GET['modal'];
    }
    $this->modal = 
    $this->options['path']   = 'msform/' . $id . '/%step';
    $this->options['next callback']   = 'Drupal\msform\msform_next';
    $this->options['back callback']   = 'Drupal\msform\msform_back';
    $this->options['return callback'] = 'Drupal\msform\msform_return';
    $this->options['finish callback'] = 'Drupal\msform\msform_finish';
    $this->options['cancel callback'] = 'Drupal\msform\msform_cancel';
    
    
    if (isset($options['cached vars']) && is_array($options['cached vars'])){
      $this->cachedVars = $options['cached vars'];
    }
    $this->state = array();
    
    MsFormRegister::add($this);
  }
  
  function _optionsDefault(){
    $form_info  = ctools_wizard_defaults($options);
  }
  
  function setChacheController(MsFormCache  $cache){
    $this->cache = $cache;
  }
  
  /**
   * 
   * @param string $name
   * @deprecated since version number
   */
  function cacheName(string $name = null){
    if ($this->_cache_name && $name){
      $this->cacheClear();
    }
    elseif ($name){
      $this->_cache_name = $name;
    }
    else {
      $this->_cache_name;
    }
  }
  /**
   * 
   * @param type $data
   * @return MsFormCache
   * @todo 
   * Удалить __CLASS__ так как в случае если открыть несколько 
   * пошаговых диалогов они презетрут друг друга  
   */
  function cache($data = null){
    if ($data){
      if (isset($this->_cache)){
        $this->cache->clear();
        $this->cache = null;
      }
      $this->_cache = new MsFormCache($this->id, $data);
    }
    else if (!isset($this->_cache)) {
      $this->_cache = new MsFormCache($this->id);
    }
    return $this->_cache;
  }
  
  function getStep(){
    
  }
  /**
   * Устанавливает текущий шаг формы
   * @param type $step
   */
  function setStep($step){
    $this->step = $step;
  }
  
  function title(){
    return ($this->step)?$this->options['order'][$this->step]:reset($this->options['order']);
  }
  
  function getState(){
    return $this->state;
  }
  
  function form($step = null){
    
    if (!isset($step) || !isset($this->options['forms'][$step])){
      // шаг не указан - получаем первый в списке 
      $this->cache()->clear();
      $step = array_keys($this->options['order'])[0];
    }
//    $this->state['ajax'] = (bool)$ajax;
    $this->step  = $step;
    
    
    // modal return переменная внутри ctools, устанавливаем в true, 
    // что бы ctools_wizard_multistep_form вызывал ajax_render и не превращал 
    // форму в набор json команд 
    // @see ctools_wizard_multistep_form
    $this->options['modal return'] = true;
    // говорим ctools  что форма ajax, что бы он подменил form action 
    // на нужный нам , тоот что указан в конструктре
    $this->state['ajax']    = $this->options['ajax'];
    $this->state['msform']  = $this;
    // @todo 
    // необходимо разобратся так как в msform_return и т.д. 
    // $form_state['msform'] !== $this, хотя должен быть 
    
    if (isset($this->cachedVars)){
      // установлены переменные $form_state, 
      // которые будут кэшироваться между всеми шагами
      // поэтому необходимо получить их их кэша и положить в form_state 
      $data = $this->cache()->getAll();
      $this->state = $this->state + $data;
    }
    ctools_include('wizard');
    $form = ctools_wizard_multistep_form($this->options, $step, $this->state);
    // так как выше установили modal return = true, то команды не 
    // будут обработаны в нутри ctools wizard. Получем их отдельно, 
    // они пригодятся пока не знаю зачем   
    //    $commands = $this->state['commands'];
    if (isset($this->state['complete']) && $this->state['complete']){
      $form = $this->result($this->state);
    }
    
    $form['#attributes']['class'] = array('msform');
    if ($this->options['ajax']){
      $form['#attributes']['class'][]  = 'ajax';
      ctools_add_js('msform', 'msform');
    }
    if (form_get_errors()) {
      // при обпработке формы были ошибки, отобржаем их
      $error_html = theme('status_messages');
      $form['_error_'] = array(
        '#markup' => $error_html, 
        '#weight' => -100000, 
      );
    }
    return $form; 
  }
  /**
   * Обработчик результата диалога, когда все обработчики форм выполены
   * @param type $form_state
   * @return array
   */
  function result(&$form_state){
    $form = array(
      'result' => array(
        '#markup' => t('Data savead'), 
      ),
    );
    return $form;
  }
  /**
   * Нужно сохранить текущий form_state, что бы восстановить 
   * его если будет нажата "Назад" в следующей форме.  
   * @param type $form_state
   */
  function next(&$form_state) {
    $this->setPressedButton(__METHOD__);
    // Много шаговая форма может использовать один и тотже 
    // конструктор на кажном шаге, т.е. form_id  будет всегда одинаков, 
    // что бы система форм не дергала постоянно одну и туже форму из кэша
    //  на следующем шаге и не происходило зацикливания, нам нужно обнулить 
    //  входные параметры $form_state['input']['form_id'], $form_state['input']['form_build_id']. 
    //  Т.е. как бы сказать системе, что форма новая 
    $form_state['input']['form_id'] = null;
    $form_state['input']['form_build_id'] = null;
    if (isset($this->cachedVars)){
      foreach($this->cachedVars as $var_name){
        $this
          ->cache()
          ->set($var_name, $form_state[$var_name]);
      }
      $this->cache()->save();
    }
  }

  function ret(&$form_state) {
    $this->setPressedButton(__METHOD__);
    $this->cache()->clear();
  }

  function back(&$form_state) {
    $this->setPressedButton(__METHOD__);
    $form_state['input']['form_id'] = null;
    $form_state['input']['form_build_id'] = null;
  }

  function cancel(&$form_state) {
    $this->setPressedButton(__METHOD__);
    $this->cache()->clear();
  }

  function finish(&$form_state) {
    $this->setPressedButton(__METHOD__);
    $id = $this->getId();
//    MsFormRegister::clear($id);
    $this->cache()->clear();
  }
}


function msform_next(&$form_state) {
  $form_state['msform']->test3 = '333333';
  $form_state['msform']->next($form_state);
}

function msform_return(&$form_state) {
  $form_state['msform']->test3 = '333333';
  $form_state['msform']->ret($form_state);
}

function msform_back(&$form_state) {
  $form_state['msform']->test3 = '333333';
  $form_state['msform']->back($form_state);
}

function msform_cancel(&$form_state) {
  $form_state['msform']->test3 = '333333';
  $form_state['msform']->cancel($form_state);
}

function msform_finish(&$form_state) {
  $form_state['msform']->test3 = '333333';
  $form_state['msform']->finish($form_state);
  
}




 