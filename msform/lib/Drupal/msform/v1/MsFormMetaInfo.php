<?php
/**
 * @file 
 *  Библиотека обертка вокруг ctools_wizard_multistep_form для создания 
 *  пошаговых форм используя принцыпы ООП
 * @author 
 *  A. Bratko <bratko_@mail.ru>
 */

/**
 * @defgroup  <test_group> (group title)
 *  Описаниетестовой группы 
 * 
 */

namespace Drupal\msform\v1;


class MsFormMetaInfo{
  
  function __construct() {
    $msform_info = array(
//      'msform controller'   => 'Drupal\icrsession\IcrLogin', 
//      'cache controller'    => 'Drupal\msform\MsFormCache', 
      'show trail'  => false, 
      'show back'   => false,
      'show cancel' => TRUE,
      'show return' => false,
      'order' => $order, 
      'forms' => $forms, 
      'cancel path' => $base_url, 
      'return path' => $base_url, 
    );;
  }
  
}
  
 