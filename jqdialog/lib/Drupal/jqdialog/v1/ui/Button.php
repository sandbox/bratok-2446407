<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Drupal\jqdialog\v1\ui;

class Button{
  protected $title    = null;
  protected $url      = null;
  protected $options  = null;
  protected $dialog   = null;
  protected $jsIsInited = null;
  protected $class    = null;
  protected $id       = null;


  static function newInstance($title = null, $url = null, $options = null,  $dialog = null){
    $btn = new static();
    $btn
      ->setTitle($title)
      ->setUrl($url);
   
    return $btn;
  }
  
  function getId() {
    return $this->id;
  } 
  
  function setId($id) {
    $this->id = $id;
    return $this;
  } 
  
  function setTitle($title){
    $this->title = $title;
    return $this;
  }
  
  function getTitle(){
    return $this->title ;
  }
  
  function setUrl($url){
    $this->url = $url;
    return $this;
  }
  
  function getUrl(){
    if ($this->url){
      return $this->url;
    }
    $url = null;
    $dialog = $this->getDialog();
    if ($dialog){
      $url = $dialog->getUrl();
    }
    return $url;
  }
  
  function getOptions(){
    $class =  $this->getClass();
    return array('attributes'=> array('id'=>$this->getId(), 'target'=>'_blank', 'class' => $class),);
  }
  
  function setDialog($dialog){
    $this->dialog =  $dialog;
    return $this;
  }
  
  function getDialog(){
    return $this->dialog;
  }
  
  static function initJsLibrary() {
    static $isCalled  = false;
    if (!$isCalled){
      $path = drupal_get_path('module', 'jqdialog');
      drupal_add_js($path.'/js/v1/ui/button.js', 'file');
      $isCalled = true;
    }
  }
  
  public function initJs() {
    static::initJsLibrary();
    if ($this->getDialog()){
      $this->getDialog()->initJs();
    }
  }
  
  function setOpenEvent($eventName){
    $this->openEvent =  $eventName;
    Return $this;
  }
  
  function addClass($class){
    $this->class[$class] = true;
    return $this;
  }
  
  function hasClass($class){
    return isset($this->class[$class]);
  }
  
  function rmClass($class){
    unset($this->class[$class]);
    return $this;
  }
  
  protected function getClass(){
    return array('icrui-jqbutton');
  }
  
  function toHtml(){
    $this->initJs();
    $url = $this->getUrl(); 
    $options = $this->getOptions();
    $options = $options + $url;
    $html = l(
      $this->getTitle(), 
      $url['path'], 
      $options 
    );
    return $html; 
  }
  
  function toDrupal(){
    
  }
  
  function toMenuItem(){
    return array('href' => 'damenu/node/9', 'title' => '#content_id', 'attributes' => array(
          'class' => array('test1'), 
          'target' => array('#content_id'),
        )); 
  }
}
