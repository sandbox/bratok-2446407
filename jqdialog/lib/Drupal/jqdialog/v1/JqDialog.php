<?php
/**
 * @file 
 *  Библиотека обертка вокруг ctools_wizard_multistep_form для создания 
 *  пошаговых форм используя принцыпы ООП
 * @author 
 *  A. Bratko <bratko_@mail.ru>
 */

/**
 * @defgroup  <test_group> (group title)
 *  Описаниетестовой группы 
 * 
 */

namespace Drupal\jqdialog\v1;

use Drupal\jqdialog\v1\UiEvent as UiEvent;

class JqDialog{
  
  /**
   * Идентификатор окна 
   * @var type 
   */
  protected $id = null;
  /**
   * Общие для всех окон настройки по умодчанию 
   * @var type 
   */
  static $deafultOption = array(
//    'appendTo'  => null, 
    'autoOpen'  => false, 
//    'buttons'   => null, 
//    'closeOnEscape' => null, 
//    'closeText'   => null, 
//    'dialogClass' => null,
//    'draggable'   => null,  
//    'height'      => null, 
//    'hide'        => null, 
//    'maxHeight'   => null, 
//    'maxWidth'    => null, 
//    'minHeight'   => null, 
//    'minWidth'    => null, 
    'modal'       => true, 
//    'position'    => array(
//      'my' => "center",
//      'at' => "center",
//      'of' => 'window'), 
    'resizable'   => false, 
//    'show'        => null, 
//    'title'       => null, 
    'width'       => 'auto', 
  );
  
  protected $options      = array();
  protected $contentUrl   = null;
  protected $content      = null;
  protected $title        = null;
  protected $controller   = null;
  
  function __construct($options = null) {
//    $this->options = (!$options)
//      ? self::$deafultOption
//      : array_merge_recursive(self::$deafultOption, $options);
    $this->options = $options;
  }
  
  function setOption($name, $value){
    $this->options[$name] = $value; 
    return $this;
  }
  
  function getOption($name){
    return isset($this->options[$name])?:null;
  }
  
  static function newInstance($param) {
    return new self();
  }
  
  function setContent($content){
    $this->content = $content;
    return $this;
  } 
  
  function getContent(){
    return $this->content;
  } 
  
  function setTitle($title){
    $this->title = $title;
    return $this;
  } 
  
  
  function commandOpen(){
    return array(
      'command' => 'jqdialogOpen', 
      'coommandOptions' => $this->toJson(), 
    );
  }
  
  function getOptions(){
    return $this->options;
  }
    
  function initJs(){
    $controller = $this->getController();
    $controller::initJsLibrary();
    self::initJsDefaultOptions();
    $settings['jqdialog'][$this->getId()] = $this->getOptions();
    drupal_add_js($settings, 'setting');
    return $this;
  }
  
  static function initJsDefaultOptions(){
    static $isCalled = false;
    if (!$isCalled){
      $settings['jqdialog']['_defaultOptions'] = self::$deafultOption;
      drupal_add_js($settings, 'setting');
      $isCalled = true;
    }
  }
    
  function setController($controller){
    $this->controller = $controller;
    return $this;
  }
  
  function getController(){
    return $this->controller;
  }
  
  function getUrl(){
    $url = $this
      ->getController()
      ->getUrl($this);
    $contentUrl = $this->getContent()->getUrl();
    $contentUrl['path'] = $url . '/' . $contentUrl['path'];
    return $contentUrl;
  }
  
  function getId(){
    if (!$this->id){
      $this->id  = uniqid();
    }
    return $this->id;
  }
  
  function setId($id){
    $this->id = $id;
    return $this;
  }
  
  function setCloseEvent(){
    if (!$this->id){
      $this->id  = uniqid();
    }
    return $this->id;
  }
  
  /**
   * Прикрепляет обработчик  $eventHandler к событию $event 
   * после события $attachAfter с дополнительными даными $data
   *  
   * @param UiEvent $event
   *  Объект события которое необходимо обработать 
   * @param type $eventHandler
   *  имя метода обработчика события $event
   * @param type $data
   *  Дополнительные даные для обработчика события 
   * @param type $attachAfterHdName   
   *  имя обрарбаботчика, после кторого должен быть 
   *  выполнен обработчик $eventHandler
   * @param type $attachAfterHdOwner -
   *  объект-владелец обработчикка $attachAfterHdName, 
   *  если не указан то $eventHandler будет прикреплен после 
   *  первого прикрепления $attachAfterHdName
   * @return \Drupal\jqdialog\v1\JqDialog
   * @todo 
   *  Перенести в родительский класс или трйет 
   */
  function on(UiEvent $event, $eventHandler, $data = array(), $attachAfterHdName = null, $attachAfterHdOwner = null){
    $eventName = (is_object($event))
      ? $event->getName()
      : $event;
    $eventGenerator = $event->getTarget(); 
    
    $data = $data + array(
      'eventGenerator' => $eventGenerator, 
    );
    if ($attachAfterHdName){
      $data['attachAfter'] = (object)array(
        'handlerName' => $attachAfterHdName, 
        'handlerOwner' => is_object($attachAfterHdOwner) ? $attachAfterHdOwner->getId() : $attachAfterHdOwner, 
      );
    }
    $this->options['eventHandlers'][$eventName][$eventHandler] = $data;
    return $this;
  }
  
  function off(string $event, string $eventHandler = null){
    return $this;
  }
  
}

 