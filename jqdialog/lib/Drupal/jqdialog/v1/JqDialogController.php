<?php
/**
 * @file 
 *  Библиотека обертка вокруг ctools_wizard_multistep_form для создания 
 *  пошаговых форм используя принцыпы ООП
 * @author 
 *  A. Bratko <bratko_@mail.ru>
 */

/**
 * @defgroup  <test_group> (group title)
 *  Описаниетестовой группы 
 * 
 */

namespace Drupal\jqdialog\v1;

class JqDialogController {
  
  protected $dialogCollection = array();
  static protected $selfInstance = null;
  static function getInstance(){
    if (!static::$selfInstance){
      static::$selfInstance = new static();
    }
    return static::$selfInstance;
  }
  protected function __construct() {
  }
    
  
  function newDialog($options = null, $className = 'Drupal\jqdialog\v1\JqDialog'){
    $dialog = new $className($options);
    $dialog->setController($this);
    $dialogId = $dialog->getId();
    $this->dialogCollection[$dialogId] = $dialog;
    return $dialog;
  }
  
  function getBasePath(){
    return 'jqdialog';
  }
  
  function getUrl($dialog){
    $base = $this->getBasePath();
    return $base . '/' . $dialog->getId();
  }
  
  static function initJsLibrary(){
    static $isCalled  = false;
    if (!$isCalled){
      drupal_add_library('system','drupal.ajax');
      drupal_add_library('system', 'ui.dialog');
      $path = drupal_get_path('module', 'jqdialog');
      drupal_add_js( $path.'/js/v1/jqdialog.js', 'file');
      drupal_add_js( $path.'/js/v1/ui/uievent.js', 'file');
      $isCalled  = true;
    }
  }
  
  function setDialogId($dialogId){
    $this->dialogId = $dialogId;
    return $this;
  }
  
  function setContentUrl($contentUrl){
    $this->contentUrl = $contentUrl;
    return $this;
  }
  function getContentUrl(){
    return  $this->contentUrl ;
  }
  
  
  function getDialogId(){
    return $this->dialogId;
  }
  
  function getDialog($id = null){
  }
  
  function getCommand($commandName, $options = null){
    $command = array(
      'command' => 'jqdialogCommands',
      'dialogId' => $this->getDialogId(), 
      'commandName' => $commandName, 
      'commandArgs' => $options,
    );
    return $command;
  }
  
  
  
}

 