var Drupal = Drupal || {};
(function($){

  var Controller = function(){
    
  }
  
  Controller.prototype.dialogCollection = [];
  Controller.prototype.globalSettings   = {};
  Controller.prototype.active  = null;
  
  Controller.prototype.commandsHandler  = function(ajaxObj, response, status){
    if (Boolean(response.commandName) && Boolean(this[response.commandName])) {
      this[response.commandName](response);
    }
  }
  
  
  Controller.prototype.getDialog  = function(dialogId){
    if (!Boolean(this.dialogCollection[dialogId])){
      this.initDialog(dialogId);
    }
    return this.dialogCollection[dialogId];
  }
  
  Controller.prototype.initDialog  = function(dialogId){
    var globalSettings = this.getGlobalSettings();
    var dialogSettings =  $.extend( {}, globalSettings['_defaultOptions'], globalSettings[dialogId]);
//    var dialogSettings = globalSettings['_defaultOptions'];
    dialogSettings.controller = this;
    this.dialogCollection[dialogId] = $(this.getElementOwner(dialogId)).jqDialog(dialogSettings);
    return this;
  }
  
  Controller.prototype.getElementOwner  = function(dialogId){
    var element = $('#' + dialogId);
    if (!element.length){
      element = $('<div id="' + dialogId + '"></div>').appendTo('body'); 
    }
    return element;
  }
  
  
  
  Controller.prototype.getGlobalSettings = function(){
   return this.globalSettings['jqdialog'];
  }
  
  Controller.prototype.setGlobalSettings = function(settings){
    this.globalSettings = settings;
    return this;
  }
  
  Controller.prototype.setActive  = function(dialog){
    this.activeDialogId = dialog.getId();
    return this;
  }
  
  Controller.prototype.getActive  = function(){
    return this.dialogCollection[this.activeDialogId] || null;
  }
  
  
  Controller.prototype.jqdialogOpen = function(options){  
  }
  
  Controller.prototype.jqdialogClose  = function(){}
  
  Controller.prototype.jqdialogUpdate = function(response){
    var element = this.getDialog(response.dialogId);
    var test = $(element).parent('.ui-dialog');
    element
      .jqDialog('open')
      .jqDialog('setContent', response.commandArgs.content);
    var v_position = 'center';
    if ($(window).height() < element.parent('.ui-dialog').height()) v_position = 'top';
    element.parent('.ui-dialog').position({
      my: "center " + v_position,
      at: "center " + v_position,
      of: window
    }); 
    var settings = this.settings || Drupal.settings;
    Drupal.attachBehaviors(element, settings);
  }
  
  var jqDialog = {};
  jqDialog.open = function(){
    return this._super();
  }
  
  jqDialog.closeEventHandler = function(event, tiggerData){
    if (event.data.eventGenerator == $(event.target).attr('id')){
      this.close();
    }
  }
  
  jqDialog.reinitPosition  = function(event, tiggerData){
    if (event.data.eventGenerator != $(event.target).attr('id')){
      return true;
    }
    var windowHeight = $(window).height();
    var bodyHeight = $('body').height();
    var ofParam  = windowHeight > bodyHeight ? $(window) : $('body');
    var dialogContainer =  this.element.parent('.ui-dialog');
    
    var position = dialogContainer.height() > ofParam.height() 
      ? 'top'
      : 'center';
    
    dialogContainer.position({
      my: "center " + position,
      at: "center " + position,
      of: ofParam
    }); 
//    console.log($(window).height());
//    console.log($('body').height());
//    console.log('reinitPosition');
  }
  
  jqDialog._create =  function() {
    this._onEventHandler();
    this._super();
  }
  
  jqDialog._onEventHandler = function(){
    var options = this.options;
    if (Boolean(options.eventHandlers)){
      Drupal.UiEventController.onEventHandlers(this, options.eventHandlers) 
    }
  }  
  
  jqDialog.isOpen = function(){
    return true;
  }
  
  jqDialog.setContent= function(content){
    $(this.element).html(content);
    return this; 
  }
  
  jqDialog.getId = function(){
    return $(this.element).attr('id');
  }
  
  jqDialog.close = function(){
    var settings = this.settings || Drupal.settings;
    Drupal.detachBehaviors(this.element, settings);
    return this._super();
  }
  
  
  $.widget('custom.jqDialog', $.ui.dialog, jqDialog);
  Drupal.jqController = new Controller();
  
  
  Drupal.behaviors.jqDialog = {
    attach: function (context, settings) {
      if (!Boolean(Drupal.ajax.prototype.commands.jqdialogCommands)){
        Drupal.ajax.prototype.commands.jqdialogCommands = $.proxy(Drupal.jqController.commandsHandler, Drupal.jqController);
        
        Drupal.jqController.setGlobalSettings(settings);
      }
    }
  }
})(jQuery)

