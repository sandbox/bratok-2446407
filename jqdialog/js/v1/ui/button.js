var Drupal = Drupal || {};
(function($){
  
  var jqButton = {} 
  jqButton.options =  {};
  jqButton._create = function() {
    this._initAjaxRequest()
  }
  
  jqButton._initAjaxRequest = function( ){
    var element_settings = {};
    element_settings.url    = this.getHref();
    element_settings.event  = this.getEventName();
    var base = this.getId();
    this.ajaxObj = Drupal.ajax[base] = new Drupal.ajax(base, this.element, element_settings);
  }
  
  jqButton.getHref = function(){
    return $(this.element).attr('href') || null;
  }

  
  jqButton.getId = function(){
    if (!Boolean(this.id)){
      var date = new Date();
      this.id = $(this.element).attr('id') || date.getTime();
    }
    return this.id;
  }

  jqButton.getEventName = function(){
    return $(this.element).attr('data-eventname') || 'click';
  }

  
  $.widget('custom.jqButton', jqButton);
  
  Drupal.jqButton = Drupal.jqButton || {};
  Drupal.behaviors.jqButton = {
    attach: function (context, settings) {
      $('a.icrui-jqbutton:not(.icrui-jqbutton-processed)', context).once('icrui-jqbutton',  function(index, obj){
//        Drupal.jqButton[button.getId()] = $(obj).jqButton();
        Drupal.jqButton[$(obj).attr('id')] = $(obj).jqButton();
      });
    }
  }
  
  
})(jQuery)

