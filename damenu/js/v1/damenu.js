/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function($){
  
  var Damenu = {}
  Damenu.options = {}
  Damenu.getId = function(){
    return $(this.element).attr('id');
  }
  
  Damenu._create  = function(){
    $(this.element).on('click', 'a', function(event){
      event.preventDefault();
    });
    this.contentContainer = $(this.options.contentContainer);
    var active = $('a:eq(0)', this.element); 
    this._initAjaxRequest()._onEventHandler().setActiveItem(active);
  }
  
  Damenu._setOptions = function( options ) {
    this._super( options );
//    this.refresh();
  },
  
  Damenu._initAjaxRequest = function( ){
    var element_settings = {};
    element_settings.url    = this.getBaseUrl();
    element_settings.event  = this.options.event;
   
    var thisId = this.getId();
    this.ajaxObj = Drupal.ajax[thisId] = new Drupal.ajax(thisId, this.element, element_settings);
    
    var damenu = this;
    this.ajaxObj.eventResponse = function(element, event){
      // здесь this - это объект класса Drupal.AJAX
      damenu.setActiveItem(event.target);
      this.options.url = damenu.getBaseUrl()
        + damenu.prepareItemUrl($(event.target).attr('href'));
      Drupal.ajax.prototype.eventResponse.call(this, element, event); 
    }
    return this;
  }
  
  Damenu.prepareItemUrl= function(url){
    url = url.replace(Drupal.settings.pathPrefix, '');
    return url;
  }
  
  Damenu.getBaseUrl = function(){
    return this.options.baseUrl;
  }
  
  Damenu.setActiveItem = function(element){
    var prev = this.getActiveItem();
    $(prev).removeClass('damenu-item-active'); 
    this.activeItem = element;
    $(element).addClass('damenu-item-active'); 
    return this;
  }
  
  Damenu.getActiveItem = function(element){
    return this.activeItem
  }
  
  Damenu.getContentContainer = function(){
    var contentContainer = null;
    var activeIem = this.getActiveItem();
    var target = $(activeIem ).attr('target');
    contentContainer = (Boolean(target) && $(target).length)
      ? $(target)
      : this.contentContainer;
    return contentContainer;
  }
  
  
  
  Damenu._onEventHandler = function(){
    var options = this.options;
    if (Boolean(options.eventHandlers)){
      Drupal.UiEventController.onEventHandlers(this, options.eventHandlers) 
    }
    return this;
  }
  
  Damenu._offEventHandler = function(){
    var options = this.options;
    if (Boolean(options.eventHandlers)){
      Drupal.UiEventController.offEventHandlers(this, options.eventHandlers) 
    }
  }
  
  Damenu.setContentHandler = function(event, tiggerData){
    if (event.data.eventGenerator != $(event.target).attr('id')){
      return true;
    }
    var cc = this.getContentContainer();
    var effect = this.options.effect;
    var duration = this.options.duration;
    $(cc).hide().html(tiggerData.content).effect(effect, duration);
    Drupal.attachBehaviors($(cc), Drupal.settings);
  }
  
  $.widget('custom.daMenu', Damenu)
  Drupal.behaviors.Damenu = {
    attach: function (context, settings) {
      $('ul.icrui-damenu:not(.icrui-damenu-processed)', context).once('icrui-damenu',  function(index, obj){
        var id = $(obj).attr('id');
        var widgetsettings = settings.damenu[id] || null;
        var damenu = $(obj).daMenu(widgetsettings); 
//        Drupal.Damenu[damenu.getId()] = damenu;
      });
    }
  }  
})(jQuery)

