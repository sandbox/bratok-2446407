<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Drupal\damenu\v1;

use \Drupal\jqdialog\v1\UiEvent as UiEvent;
use Drupal\icrform\v3\IcrMsForm as IcrMsForm;

class Damenu{
  static $basePath = 'damenu';

  protected $theme = null;
  protected $items = array();
  protected $options = array(
    'event' => 'click'
  );
   
  function setItem($url, $title, $targetId = null, $options = array()){
    $this->items[$url] = array(
      'title'   => $title, 
      'target'  => $targetId, 
      'options' => $options,
    );
    return $this;
  }
  
  function getItem($url){
//    return $this->
  } 
  
  function unsetItem($url){
    
  }
  
  function setTargetId($targetId){
    
  }
  
  
  function render(){
    
  }
  
  function setContentContainerSelector($jqSelector){
    $this->options['contentContainer'] = $jqSelector;
    return $this;
  }
  
  function getContentContainerSelector(){
    return isset($this->options['contentContainer']) 
      ? $this->options['contentContainer'] 
      : null;
  }
  
  function getId(){
    if (!$this->id){
      $this->id  = uniqid();
    }
    return $this->id;
  }
  
  function getOptions(){
    if (!isset($this->options['baseUrl'])){
      $this->options['baseUrl'] = $this->getBaseUrl();
    }
    //Дефолтные настройки
    if (!isset($this->options['effect'])){
      $this->options['effect'] = 'fade';
    }
    if (!isset($this->options['duration'])){
      $this->options['duration'] = 3000;
    }
    return $this->options;
  }
  
  function setOptions($options) {
    $this->options = array_merge($this->options, $options);
    return $this;
  }

  static function getEvent($eventName, $damenuId, $data = null) {
    $eventName = 'Damenu.' . $damenuId . '.' . $eventName;
    $event = UiEvent::getInstance($eventName, $damenuId, $data);
    return $event;
  }
  
  function getBaseUrl(){
    return url(static::$basePath . '/' . $this->getId());
  }
  
  function setBaseUrl($url){
    $this->options['baseUrl'] = $url;
    return $this;
  }
  
  function on(UiEvent $event, $eventHandler, $data = array(), $attachAfterHdName = null, $attachAfterHdOwner = null){
    $eventName = (is_object($event))
      ? $event->getName()
      : $event;
    $eventGenerator = $event->getTarget(); 
    
    $data = $data + array(
      'eventGenerator' => $eventGenerator, 
    );
    if ($attachAfterHdName){
      $data['attachAfter'] = (object)array(
        'handlerName' => $attachAfterHdName, 
        'handlerOwner' => is_object($attachAfterHdOwner) ? $attachAfterHdOwner->getId() : $attachAfterHdOwner, 
      );
    }
    $this->options['eventHandlers'][$eventName][$eventHandler] = $data;
    return $this;
  }
  
  function toDrupal(){
    drupal_add_library('system','drupal.ajax');
    drupal_add_library('system', 'ui.dialog');
    $path = drupal_get_path('module', 'jqdialog');
    drupal_add_js( $path.'/js/v1/ui/uievent.js', 'file');
    $thisId = $this->getId();
    $this->on(static::getEvent('setContent', $thisId), 'setContentHandler');  
    $links = array();
    foreach($this->items as $url => $item){
      $links[] = array(
        'href'  => $url, 
        'title' => $item['title'], 
        'attributes' => array(
          'class'   => array('test1'), 
        )
      );  
    }
    return array(
      '#attached' => array(
        'js' => array(
          drupal_get_path('module', 'damenu'). '/js/v1/damenu.js', 
          array(
            'data' => array(
              'damenu' => array(
                $thisId => $this->getOptions()
              )
            ), 
            'type' => 'setting',
          ),
        ), 
      ), 
      '#theme' => 'links', 
      '#links' => $links,
      '#attributes' => array(
        'id' => $this->getId(),
        'class' => array('icrui-damenu'),
      ),
    );
  }
}
