<?php

namespace Drupal\damenu\v1;
use Drupal\jqdialog\v1\JqDialogController as JqDialogController;

class DamenuController extends JqDialogController{
  protected $damenuId = null;
  
  function setDamenuId($damenuId){
    $this->damenuId = $damenuId;
    return $this;
  }
  
  function getDamenuId(){
    return  $this->damenuId;
  }
  
  
}

 